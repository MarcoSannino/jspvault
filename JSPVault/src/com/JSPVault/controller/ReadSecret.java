package com.JSPVault.controller;

	import java.io.IOException;
import java.util.Properties;

	import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

	import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

	@Controller
	public class ReadSecret {

	   @RequestMapping(value = "/ReadSecret", method = RequestMethod.GET)
	   public ModelAndView secret() {
		   //codice java
	      return new ModelAndView("ReadSecret", "command", new Secret()); //mappa la pagina di ritorno
	   }
	   
	   @RequestMapping(value = "/ReadSecret", method = RequestMethod.POST) 
	   public @ResponseBody
	   String ReadaSecret(@ModelAttribute("SpringWeb")Secret secret, 
	   ModelMap model) {
		   Properties prop=new Properties();
		   try {
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   System.out.println("POST RECEIVED");
		   EntityManagerFactory j=Persistence.createEntityManagerFactory("Segreto");
		   Segreto s = j.createEntityManager().find(Segreto.class,prop.get("vault_component_name").toString());
		   System.out.println("fnaweignana: "+s.getId()+" "+s.getS());
	      model.addAttribute("path", secret.getPath());
	      String Segreto="";
		try {
			Segreto = VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+secret.getPath(),s.getS());
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      System.out.println("ciao: "+Segreto);
	      
	      return Segreto;
	   }

}
