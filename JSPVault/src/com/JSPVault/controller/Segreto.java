package com.JSPVault.controller;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Segreto
 *
 */
@Entity

public class Segreto implements Serializable {

	
	private String S;   
	@Id
	private String Id;
	private static final long serialVersionUID = 1L;

	public Segreto() {
		super();
	}   
	public String getS() {
		return this.S;
	}

	public void setS(String S) {
		this.S = S;
	}   
	public String getId() {
		return this.Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}
   
}
