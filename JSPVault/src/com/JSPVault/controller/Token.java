package com.JSPVault.controller;

public class Token {
	private String key;
	private String token;
	public Token(){
		
	}
	public Token(String a,String b){
		key=a;
		token=b;
	}
	public void setKey(String k){
		this.key=k;
	}
	public void setToken(String t){
		this.token=t;
	}
	public String getKey(){
		return this.key;
	}
	public String getToken(){
		return this.token;
	}

}
