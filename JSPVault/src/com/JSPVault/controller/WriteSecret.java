package com.JSPVault.controller;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

@Controller
public class WriteSecret {

   @RequestMapping(value = "/WriteSecret", method = RequestMethod.GET)
   public ModelAndView Wsecret() {
	   //codice java
      return new ModelAndView("WriteSecret", "command", new Secret()); //mappa la pagina di ritorno
   }
   
   @RequestMapping(value = "/WriteSecret", method = RequestMethod.POST)
   public void WriteaSecret(@ModelAttribute("SpringWeb")Secret secret, 
   ModelMap model) {
	   Properties prop=new Properties();
	   try {
		prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   System.out.println("POST RECEIVED");
	   EntityManagerFactory j=Persistence.createEntityManagerFactory("Segreto");
	   Segreto s = j.createEntityManager().find(Segreto.class,prop.get("vault_component_name").toString());
	   System.out.println("fnaweignana: "+s.getId()+" "+s.getS());
      model.addAttribute("path", secret.getPath());
      model.addAttribute("secret", secret.getSecret());
      String Segreto="";
	try {
		//Segreto = VaultApi.ReadSecret("http://127.0.0.1:8200/v1/secret/"+secret.getPath(),s.getS());
		Segreto = VaultApi.WriteSecret(prop.get("vault_secret_path").toString()+secret.getPath(), s.getS(), secret.getSecret());
	} catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      System.out.println("ciao: "+Segreto);
      
      //return "result";
   }

}
