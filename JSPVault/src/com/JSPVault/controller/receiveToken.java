package com.JSPVault.controller;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

@Controller
public class receiveToken {

   @RequestMapping(value = "/addToken", method = RequestMethod.GET,headers="Accept=*/*",produces="application/json")
   public ModelAndView token() {
	   //codice java
	   System.out.println("i'm in the get");
      return new ModelAndView("token", "command", new Token()); //mappa la pagina di ritorno
   }
   
   @RequestMapping(value = "/addToken", method = RequestMethod.POST,headers="Accept=*/*",produces="application/json")
   public @ResponseBody
   String addToken(@ModelAttribute("SpringWeb")Token token, 
   ModelMap model) {
	   Properties prop=new Properties();
	   try {
		prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/vault_conf.properties"));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   System.out.println(prop.get("vault_server").toString());
	   System.out.println("i'm in the post");
      model.addAttribute("key", token.getKey());
      model.addAttribute("token", token.getToken());
      System.out.println(token.getKey()+" "+token.getToken());
      Segreto s= new Segreto();
      s.setId(token.getKey());
      s.setS(token.getToken());
      EntityManagerFactory j=Persistence.createEntityManagerFactory("Segreto");
      EntityManager em =  j.createEntityManager();
      EntityTransaction t = em.getTransaction();
      t.begin();
      em.persist(s);
      t.commit();
      em.close();
      JSONObject nnn=new JSONObject();
      nnn.put(token.getKey(), token.getToken());
      
      return nnn.toJSONString();
   }
}
