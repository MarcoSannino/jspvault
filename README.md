# README #

La struttura dei file è un po' incasinata perché avevo problemi con la persistenza.
In ogni caso ora spiego classe per classe:
1)KeyFinder.java
E' una mia classe di utilità(vecchia,ho riusato) per lavorare sui file json, in particolare ho usato la search.
2) ReadSecret.java
è la classe che richiama l'API di read su Vault.Lavora in post ricevendo il path a cui deve leggere il segreto, legge il token da h2 e richiama l'API di vault per leggere il segreto. TODO: renderla dinamica sul server vault da cui leggere e la chiave del token associato al componente in cui viene integrato.Il ritorno del segreto ad ora è una stampa in console aspettando di sapere che tipo di file si preferisce.
3) Secret,Segreto e Token sono classi semplici...
4) VaultApi.java
è la classe che offre le funzionalità di accesso a vault.Quella che presentai la scorsa volta.
Ha una funzione doG() per accedere in get a vault
Ha una funzione doP() per accedere in post a vault
Ha una funzione ReadSecret() la quale richiama la doG() per fare il retrieve di un segreto.
Ha una funzione WriteSecret() la quale richiama la post per scrivere un segreto su vault.
5)WriteSecret
è la classe che richiama l'API di write su Vault, Lavora in post ricevendo il path a cui deve scrivere il segreto ed il segreto stesso,ottiene il token da h2 e quindi richiama la write sulle API.TO DO: anch'essa da rendere dinamica sul token e sull'indirizzo di vault
6) ReceiveToken
Mappata sul path /addToken è la classe che ha il compito di ricevere il token da POST e scriverlo su h2.La classe lavora sia in get che in post:
Comportamento POST:
Riceve in post una chiave ed il token da salvare in h2, e lo salva
Comportamento GET:
fa il retrieve di una pagina .jsp nel quale è presente una form in cui inserire un token e poi mandarlo in post al path /addToken 
io l'ho testata sia usando la form sia mandando una post da un'altra applicazione.L'ho fatto per comodità più che altro.
ps mi scuso in anticipo per il disordine XD
Un normale flusso di esecuzione è:
1) Faccio partire il server (XD)
2) Faccio una post al path http://localhost:8080/JSPVault/addToken passando nel body il token da salvare
3) Faccio una post per leggere(/JSPVault/ReadSecret) o scrivere(/JSPVault/WriteSecret)
4) Preparo la "cazziata" a Marco perché funziona tutto ma vanno aggiustate alcune cose :) (lo so devo rendere dinamico il discorso delle config e devo gestire il ritorno, ma come scritto nella prima mail aspetto di sapere come vogliamo farlo)